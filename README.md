# Responsive Video #

The Responsive-video plugin has two main functions:

* to create a video that crops itself to fit containers height and width (similar to background-size: cover for images).
* to toggle the video to play when on screen and pause when off screen.

### To include the responsive video plugin into your project: ###

* add the included css or scss into your style sheets
* add the included js into your javascript file


### to use the plugin ###

place your video in the flowing container and include the associated classes.
```
<div class="responsive-video-container">
    <video class="responsive-video" src=""></video>
</div>
```

### dependancies ###

* jQuery
