function responsiveVideo(){
  var video = $('.responsive-video');
  var videoContainer = $('.responsive-video-container');
  var vHeight = video.width();
  var vWidth = video.height();
  
  // fades in the video once it is loaded.
  function videoPlaying() {
    video.each(function(){
      var currentVideo = $(this);
      var vid = this;
      var count = 0
      var stateCheck = setInterval(function(){
        count ++;
        if(vid.readyState == 4){
          //seting video styles after load
          dimensionCheck();
          videoContainer.addClass("fade-video");
          clearInterval(stateCheck);
        } else if(count >= 300){ //prevent interval from running longer than 30seconds
          videoContainer.addClass("fade-video");
          clearInterval(stateCheck);
        }
      }, 100);
    });
  }

  // plays or pauses the video based on wether or not it is in the veiwport
  function inView() {
    video.each(function(){
      var currentVideo = $(this);
      var vid = this;
      var _window = $(window);
      var videoPositon;
      var windowTop;
      var windowBottom;
      var play = false;
      
      window.addEventListener("scroll", function(){
        windowTop = _window.scrollTop();
        windowBottom = windowTop + _window.outerHeight();
        videoPositon = currentVideo.offset().top + currentVideo.outerHeight();
        //plays the video if in viewport
        if(videoPositon >= windowTop && currentVideo.offset().top <= windowBottom) {
          if(!play){
            vid.play();
            play = true;
            console.log("play");
          }
        //puases the video if it is not in the viewport
        } else {
          if(play){
            vid.pause();
            play = false;
            console.log("pause");
          }
        }
      });
    });
  }

  // checks to see if the video is filling its container and modifies styles accordingly 
  function dimensionCheck(){
    if(video.width() < videoContainer.width()){
      video.css({
        "height": "auto",
        "width": "100%"
      });
    } else if(video.height() < videoContainer.height()) {
      video.css({
        "height": "100%",
        "width": "auto"
      });
    }
  }

  // initializing 
  inView();
  videoPlaying();
  window.addEventListener('resize', function(){
    dimensionCheck();
  });
  
}

responsiveVideo();